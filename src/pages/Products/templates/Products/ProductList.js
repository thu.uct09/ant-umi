import React, { Component } from 'react';
import { Table, Popconfirm } from 'antd';
import ProductModal from './ProductModal'

export default class ProductList extends Component {

  componentDidMount() {
    //
  }

  render() {
    const dataSource = this.props.reportData.data ? this.props.reportData.data : []

    const columns = [
      {
        title: 'Code',
        dataIndex: 'code',
        key: 'code',
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
      },
      {
        title: 'Operation',
        key: 'operation',
        render: (text, record) => (
            
          <span>
            <ProductModal record={record} onOk={this.props.editHandler.bind(null, record.code)}> 
              <a href={null}>Edit             </a>
            </ProductModal>
            <Popconfirm title="Confirm to delete?" onConfirm={this.props.deleteHandler.bind(null, record.code)}>
              <a href={null}>Delete</a>
            </Popconfirm>
          </span>
        ),
      },
    ];
    
    return (
      <div>
        <Table
          columns={columns}
          pagination={false}
          dataSource={dataSource}
          rowKey={record=> record.code}
          scroll={{ x: '100%' }}
        />
      </div>
    );
  }
}
