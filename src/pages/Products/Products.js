import React, { Component } from 'react'
import { connect } from 'dva'
import ProductList from './templates/Products/ProductList'

@connect(data => {
    return {
      reportData: data.products.current || {},

    }
})

class Product extends Component {

    constructor(props) {
        super(props)

        this.state = {
        reportType: null,
        }
    }
    componentDidMount() {
        const { dispatch } = this.props
        dispatch({
            type: 'products/fetch',
            payload: {type: 'all'}
        })
    }

    deleteHandler = (code) => {
        const { dispatch } = this.props
        dispatch({
            type: 'products/remove',
            payload: code,
        })
    }

    editHandler = (code, values) => {
        const { dispatch } = this.props
        dispatch({
          type: 'products/patch',
          payload: { code, values },
        });
    }

    render() {
        return (
            <div>
                <ProductList {...this.props} deleteHandler={this.deleteHandler} editHandler={this.editHandler}/>
            </div>
        );
    }
}

export default Product
