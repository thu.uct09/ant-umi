//import * as productService from '../services/products'

const MOCK_TRANSFER_BILLS = {
    status: 'SUCCESS',
    errors: null,
    body: {
      report: {
        data: [
          {
            code: '201955',
            name: 'Fan',
            price: '300,000',
          },
          {
            code: '201956',
            name: 'Box',
            price: '400,000',
          },
          {
            code: '201957',
            name: 'Knife',
            price: '340,000',
          },
          {
            code: '201958',
            name: 'Pillow',
            price: '500,000',
          },
        ],
      },
    },
  };

  const MOCK_TRANSFER_DEL = {
    status: 'SUCCESS',
    errors: null,
    body: {
      report: {
        data: [
          {
            code: '201955',
            name: 'Fan',
            price: '300,000',
          },
          {
            code: '201956',
            name: 'Box',
            price: '400,000',
          },
          {
            code: '201957',
            name: 'Knife',
            price: '340,000',
          }
        ],
      },
    },
  };

  const MOCK_TRANSFER_EDIT = {
    status: 'SUCCESS',
    errors: null,
    body: {
      report: {
        id: 1,
        reportName: 'Transfer Bill',
        hotel: 'EMM HUE HOTEL',
        file:
          'https://s3-ap-northeast-1.amazonaws.com/nw-kintone/content-data/output_transfer_bills.xlsx',
        data: [
          {
            code: '201955',
            name: 'Fan',
            price: '300,000',
          },
          {
            code: '201956',
            name: 'Box',
            price: '400,000',
          },
          {
            code: '201957',
            name: 'Knife',
            price: '340,000',
          },
          {
            code: '201958',
            name: 'Pillow',
            price: '700,000',
          },
        ],
      },
    },
  };
export default {
    namespace:'products',
    state: {
        list: [],
        current: null,
    },
    reducers: {
        save(state, action) {
            return { ...state, current: action.payload }
        }
    },
    effects: {
        *fetch({ payload }, {put}) {
            let response
            if (payload.type === 'all')
              response = MOCK_TRANSFER_BILLS;
            if (payload.type === 'remove')
              response = MOCK_TRANSFER_DEL;
            if (payload.type === 'edit')
              response = MOCK_TRANSFER_EDIT;
            yield put ({
                type: 'save',
                payload: response.body.report
            })
        },
        *remove({ payload: code }, { put }) {
          yield put({ type: 'fetch', payload: { type: 'remove' } })
        },
        *patch({ payload: { code, values } }, { put }) {
          yield put({ type: 'fetch', payload: { type: 'edit' } })
        },
    }
}