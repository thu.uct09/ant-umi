export default [
    {
        path: '/',
        component: '../layouts/BasicLayout' ,
        routes: [
            {
                path: '/product',
                name: 'product',
                component: './Products/Products',
            }
        ],
    }
]